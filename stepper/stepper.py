#!/usr/bin/python3
#CONTROLLING A STEPPER MOTOR
from time import sleep
import pifacedigitalio.core as pfio
from threading import Lock

pfio.init()

def anticlockwise(rotations, speed):
  print ("anticlockwise(%d,%d)" % (rotations, speed))
  sleep_time=0.1 / float(speed)
  for loop in range(1,int(512*float(rotations))):
      pfio.digital_write(4,1)
      sleep(sleep_time)
      pfio.digital_write(7,0)
      sleep(sleep_time)
      pfio.digital_write(5,1)
      sleep(sleep_time)
      pfio.digital_write(4,0)
      sleep(sleep_time)
      pfio.digital_write(6,1)
      sleep(sleep_time)
      pfio.digital_write(5,0)
      sleep(sleep_time);
      pfio.digital_write(7,1);
      sleep(sleep_time)
      pfio.digital_write(6,0)
      sleep(sleep_time)
  pfio.digital_write(7,0)

def clockwise(rotations, speed):
  print ("clockwise(%d,%d)" % (rotations, speed))
  sleep_time=0.1 / float(speed)
  for loop in range(1,int(512*float(rotations))):
    pfio.digital_write(7,1)
    sleep(sleep_time)
    pfio.digital_write(4,0)
    sleep(sleep_time)
    pfio.digital_write(6,1)
    sleep(sleep_time)
    pfio.digital_write(7,0)
    sleep(sleep_time)
    pfio.digital_write(5,1)
    sleep(sleep_time)
    pfio.digital_write(6,0)
    sleep(sleep_time)
    pfio.digital_write(4,1)
    sleep(sleep_time)
    pfio.digital_write(5,0)
    sleep(sleep_time)
  pfio.digital_write(4,0)


def button1_action(e):
  global lock
  if lock.locked():
    print("already running")
  else:
    lock.acquire()
    try:
      anticlockwise(1,10)
    finally:
      lock.release()

def button2_action(e):
  global lock
  if lock.locked():
    print("already running")
  else:
    lock.acquire()
    try:
      clockwise(1,10)
    finally:
      lock.release()

def button3_action(e):
  global lock
  if lock.locked():
    print("already running")
  else:
    lock.acquire()
    try:
      clockwise(1,600)
    finally:
      lock.release()

def button4_action(e):
  global lock
  if lock.locked():
    print("already running")
  else:
    lock.acquire()
    try:
      anticlockwise(1,600)
    finally:
      lock.release()


if __name__ == "__main__":
  import pifacedigitalio
  pifacedigital = pifacedigitalio.PiFaceDigital()
  lock = Lock()
  listener = pifacedigitalio.InputEventListener(chip=pifacedigital)
  listener.register(0, pifacedigitalio.IODIR_ON, button1_action)
  listener.register(1, pifacedigitalio.IODIR_OFF, button2_action)
  listener.register(2, pifacedigitalio.IODIR_ON, button3_action)
  listener.register(3, pifacedigitalio.IODIR_OFF, button4_action)
  listener.activate()
