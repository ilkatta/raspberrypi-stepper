#!/usr/bin/python3


VELOCITY=600
ROTATIONS=0.5


'''
CONNECTIONS:
    MOTOR #1 & #2:
		5v --- red

	MOTOR #2:
		7 --- blue
		6 --- pink 
		5 --- yellow
		4 --- orange

	MOTOR #1:
		3 --- blue
		2 --- pink
		1 --- yellow
		0 --- orange

BOTTONS:
 |   1   |   2   |   4   |   8   |
 | #2  C | #2  A | #1  C | #1  A |

'''

from time import sleep
import pifacedigitalio.core as pfio
from threading import Lock

pfio.init()

POS_CLOCK = [
 [1,0,0,1],
 [0,0,0,1],
 [0,0,1,1],
 [0,0,1,0],
 [0,1,1,0],
 [0,1,0,0],
 [1,1,0,0],
 [1,0,0,0],
]

POS_ANTI = [
 [1,0,0,1],
 [1,0,0,0],
 [1,1,0,0],
 [0,1,0,0],
 [0,1,1,0],
 [0,0,1,0],
 [0,0,1,1],
 [0,0,0,1]
]

from sys import stdout
def step(motor, sleep_time,anticlockwise=False):
	if anticlockwise:
		pos=POS_ANTI
	else:
		pos=POS_CLOCK

	inc=(4*(motor-1))
	for a in pos:
		for i in range(len(a)):
			pfio.digital_write(i+inc,a[i])
			#stdout.write("%d " % a[i] )
			sleep(sleep_time)
		#print("")
	#print (".")

def step0(motor):
	if motor is 1:
		r =range(0, 4)
	elif motor is 2:
		r = range(4, 8)
	else:
		return

	for i in r:
		pfio.digital_write(i,0)


def rotate(motor,rotations, speed,anticlockwise=False):
	print("rotate( motor=%d, rotations=%d, speed=%d, anticlockwise=%s )" % (motor, rotations, speed, str(anticlockwise)) )
	sleep_time=0.1 / float(speed)
	for loop in range( 1,int(512*float(rotations)) ):
		step(motor,sleep_time,anticlockwise)
	step0(motor)


def button_action(e):
  print("event flag: %d" % e.interrupt_flag)
  global lock1,lock2
  flag=e.interrupt_flag
  if flag is 1 or flag is 2:
    lock = lock1
    motor=2
    
  if flag is 4 or flag is 8:
    lock = lock2
    motor=1

  if flag is 1 or flag is 4:
    anticlock=False
  else:
    anticlock=True

  if lock.locked():
    print("already running")
  else:
    lock.acquire()
    try:
      rotate(motor,1,VELOCITY,anticlock)
    finally:
      lock.release()

import sys,signal
def interupt(s,f):
  global listener
  print("deinit")
  pfio.deinit()
  sys.exit(0)


if __name__ == "__main__":
  import pifacedigitalio
  pifacedigital = pifacedigitalio.PiFaceDigital()

  lock1 = Lock()
  lock2 = Lock()
  
  signal.signal(signal.SIGINT, interupt)

  listener = pifacedigitalio.InputEventListener(chip=pifacedigital)
  listener.register(0, pifacedigitalio.IODIR_OFF, button_action)
  listener.register(1, pifacedigitalio.IODIR_OFF, button_action)
  listener.register(2, pifacedigitalio.IODIR_OFF, button_action)
  listener.register(3, pifacedigitalio.IODIR_OFF, button_action)
  listener.activate()
