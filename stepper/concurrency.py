#!/usr/bin/python3
# test concurrency 
from time import sleep
import pifacedigitalio.core as pfio
from multiprocessing import Lock
from pifacedigitalio import PiFaceDigital

pfio.init()
pifacedigital = PiFaceDigital()
running=False
count=0


def do_something(pled):
  pifacedigital.leds[pled-1].set_high() 
  sleep(3)
  pifacedigital.leds[pled-1].set_low()



def button_action(e):
  flag=e.interrupt_flag
  global lock1,lock2
  if flag > 2:
    lock = lock2
  else:
    lock = lock1

  if not lock.acquire(False):
    print("already running")
  else:
    try:
      print ("thread %d start" % flag)
      do_something(flag)
      print ("thread %d stop" % flag)
    finally:
      lock.release()


def interupt(s,f):
  global listener
  print("deinit")
  pifacedigitalio.deinit()
  sys.exit(0)


if __name__ == "__main__":

  import pifacedigitalio,sys,signal
  pifacedigital = pifacedigitalio.PiFaceDigital()

  lock1 = Lock()
  lock2 = Lock()

  signal.signal(signal.SIGINT, interupt)

  listener = pifacedigitalio.InputEventListener(chip=pifacedigital)
  listener.register(0, pifacedigitalio.IODIR_ON, button_action)
  listener.register(1, pifacedigitalio.IODIR_OFF, button_action)
  listener.register(2, pifacedigitalio.IODIR_ON, button_action)
  listener.register(3, pifacedigitalio.IODIR_OFF, button_action)
  listener.activate()
